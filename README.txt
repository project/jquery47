
OVERVIEW

Drupal 5.0 will include the fab JavaScript library jQuery. But where does that
leave us poor slobs who need to develop on 4.7? Out in the cold, that's where.
Sadly, Drupal 4.7's own JavaScript package conflicts with jQuery in a number of
places, preventing easy including of the jQuery library in contributed modules.

Enter jquery47, a slightly hacked version of jQuery, compatible with Drupal's
own JS files. By invoking jquery_add_js('path/to/your/file.js') you can reap the
benefits of jQuery while wrapped warmly in the bosom of Drupal 4.7

USING

First, a note: the make jQuery compatible with Drupal, I had to rename the $
function to JQ. Sorry, there's really no way around this. When Drupal 5.0 comes
out, you'll have to rename all your calls to JQ('...') to $('...').

Write your JS file and put it somewhere. Using drupal_get_path() get your file's
path. Call jquery_add_js($yourfilepath) and you're ready to go.

CONTENTS

In addition to the jquery47.module, there is also two versions of the modified
jQuery library. The normal human readable version for you to see what crimes I
committed agains the jQuery library, and a compressed version to save you
bandwidth $$$.

EPILOGUE

This module will be deprecated in Drupal 5.0 (thank god). Use at your own risk
until then.

AUTHOR
Mark Fredrickson
mark@advantagelabs.com
